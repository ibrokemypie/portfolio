import fullpage from 'fullpage.js';
import "./styles.scss";
import wen1 from './img/wen 1.png';
import wen2 from './img/wen 2.png';
import wen3 from './img/wen 3.png';

function img(img) {
	var image = new Image();
	image.src = img;
	return image;
}

function nav() {
	function navlink(name) {
		var link = document.createElement("a");
		link.classList.add("navlink");
		link.dataset.menuanchor = name;
		link.innerHTML = name;
		link.href = "#" + name;

		return link;
	}

	var navbar = document.createElement("nav");
	navbar.id = "navbar";

	navbar.appendChild(navlink("app"));
	navbar.appendChild(navlink("web"));
	navbar.appendChild(navlink("photography"));
	navbar.appendChild(navlink("home"));

	return navbar;
}

function fpwrapper() {
	function main() {
		var section = document.createElement('div');
		section.classList.add("section");
		section.id = "main";

		var intro = document.createElement('div');
		intro.id = "intro";

		var title = document.createElement('span');
		title.id = "title";
		title.textContent += "ibrokemypie design";

		var body = document.createElement("p");
		body.innerText = "photographer, developer, web designer and some times apps too.";

		intro.appendChild(title);
		intro.appendChild(body);
		section.appendChild(intro);

		return section;
	}

	function web() {
		var section = document.createElement('div');
		section.classList.add("section");
		section.id = "webdev";

		var c1 = document.createElement('div');
		var w1 = img(wen1);
		c1.appendChild(w1);
		c1.classList.add("slide");

		var c2 = document.createElement('div');
		var w2 = img(wen2);
		c2.appendChild(w2);
		c2.classList.add("slide");
		
		var c3 = document.createElement('div');
		var w3 = img(wen3);
		c3.appendChild(w3);
		c3.classList.add("slide");

		section.appendChild(c1);
		section.appendChild(c2);
		section.appendChild(c3);

		return section;
	}

	function photography() {
		var section = document.createElement('div');
		section.classList.add("section");
		section.id = "photos";

		return section;
	}

	var fullpage = document.createElement('div');
	fullpage.id = "fullpage";
	
	fullpage.appendChild(main());
	fullpage.appendChild(web());
	fullpage.appendChild(photography());

	return fullpage;
}

function rest() {
	var area = document.createElement('div');
	area.innerText = "some stuff";
	return area;
}

document.title = "ibrokemypie";

document.body.appendChild(fpwrapper());
document.body.appendChild(nav());
// document.body.appendChild(rest());

var fullPageInstance = new fullpage('#fullpage', {
	menu: "#navbar",
	scrollBar: true,
	license: "GPLv3",
	autoScrolling:true,
	anchors: ["home", "web", "photography", "app"],
});